﻿using EjercicioLogin.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EjercicioLogin.Controllers
{
    public class LoginController : Controller
    {
        //declarar variable de acceso a modelo(BD)
        private LoginContext db = new LoginContext();

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Usuario, string Password)
        {
            var consulta = (from u in db.USUARIOS_Nombre
                            where u.Usuario == Usuario
                                    && u.Password == Password
                            select u);

            if (consulta.Any())
            {
                Session["usuario"] = consulta.FirstOrDefault().Usuario;
                return RedirectToAction("CrearUsuario");
            }

            ViewBag.Error = "Usuario y/o contraseña incorrecto, o usuario inactivo";
            return View();
        }


        public ActionResult CrearUsuario()
        {
            var genero = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE GENERO"},
                                              new {ID="M",Name="MASCULINO"},
                                              new{ID="F",Name="FEMENINO"},
                                          },
               "ID", "Name", 1);
            ViewData["generos"] = genero;
            return View();
        }


        [HttpPost]
        public string CrearUsuario(string Usuario, string Password, string Genero, string Correo, bool Activo)
        {
            var genero = new SelectList(new[]
                                          {
                                              new {ID="",Name="--SELECCIONE GENERO"},
                                              new {ID="M",Name="MASCULINO"},
                                              new{ID="F",Name="FEMENINO"},
                                          },
               "ID", "Name", 1);
            ViewData["generos"] = genero;

            if (ModelState.IsValid)
            {
                try
                {
                    var result = db.sp_insertar_usuario(Usuario, Password, Correo, Genero, Activo);
                    db.SaveChanges();
                    if (result == -1)
                    {
                        ViewBag.Error = "Usuario ya fue dado de alta";
                        return "OK";
                    }
                    return "ER";
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Error" + ex.InnerException;
                    return "ER";
                }
            }
            return "OK";
        }




        //Listar Usuarios
        public ActionResult ListarUsuario()
        {
            var lusuarios = db.sp_listar_usuarios();
            List<USUARIOS_Nombre> usuarios = new List<USUARIOS_Nombre>();

            foreach (var u in lusuarios)
            {

                usuarios.Add(new USUARIOS_Nombre
                {
                    ID = u.ID,
                    Usuario = u.Usuario,
                    Password = u.Password,
                    Correo = u.Correo,
                    Genero = u.Genero,
                    CadenaActivo = u.Activo
                });
            }

            return PartialView("_ListarUsuarios", usuarios);
        }


        //Vista para inactivar usuario
        public ActionResult Eliminar(int id)
        {
            return View(db.USUARIOS_Nombre.Find(id));
        }

        //update del campo activo del usuario
        [HttpPost]
        public ActionResult Eliminar(USUARIOS_Nombre usuario)
        {
            db.Database.ExecuteSqlCommand("UPDATE USUARIOS_Nombre set Activo='False' WHERE ID=@usuario", new SqlParameter("@usuario", usuario.ID));
            db.SaveChanges();
            return RedirectToAction("CrearUsuario");
        }
    }
}