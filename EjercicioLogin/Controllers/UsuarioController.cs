﻿using EjercicioLogin.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EjercicioLogin.Controllers
{
    public class UsuarioController : Controller
    {
        //declarar variable de acceso a modelo(BD)
        private LoginContext db = new LoginContext();

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Usuario, string Password)
        {
            var consulta = (from u in db.USUARIOS_Nombre where u.Usuario == Usuario 
                           && u.Password==Password select u);

            if (consulta.Any())
            {
                Session["usuario"] = consulta.FirstOrDefault().Usuario;
                return RedirectToAction("Index");
            }

            ViewBag.Error = "Usuario y/o contraseña incorrecto, o usuario inactivo";
            return View();
        }


        //Listar Usuarios
        public ActionResult Index()
        {
            var lusuarios = db.sp_listar_usuarios();
            List<USUARIOS_Nombre> usuarios = new List<USUARIOS_Nombre>();

            foreach(var u in lusuarios)
            {

                usuarios.Add(new USUARIOS_Nombre {
                    ID = u.ID,
                    Usuario = u.Usuario,
                    Password = u.Password,
                    Correo = u.Correo,
                    Genero = u.Correo,
                    CadenaActivo = u.Activo
                });
            }

            return View(usuarios);
        }



        public ActionResult CrearUsuario()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CrearUsuario(string Usuario,string Password, string Genero, string Correo, bool Activo)
        {
            if (ModelState.IsValid)
            {
                    try
                    {
                        var result = db.sp_insertar_usuario(Usuario, Password, Correo, Genero, Activo);
                        db.SaveChanges();
                        if (result == -1)
                        {
                            ViewBag.Error = "Usuario ya fue dado de alta";
                            return View();
                        }
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Error" + ex.InnerException;
                        return View();
                    }
            }
            return View();
        }



        public ActionResult Eliminar(int id)
        {
            return View(db.USUARIOS_Nombre.Find(id));
        }


        [HttpPost]
        public ActionResult Eliminar(USUARIOS_Nombre usuario)
        {
            db.Database.ExecuteSqlCommand("UPDATE USUARIOS_Nombre set Activo='False' WHERE ID=@usuario", new SqlParameter("@usuario", usuario.ID));
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}